# Wardrobify

Team:

* Cyrus - Hats
* Craig - Shoes

## Design

## Shoes microservice

_Explain your models and integration with the wardrobe
microservice, here._

I'm going to create a model for a shoe inside of my shoe microservice.
The model is going to contain values for, manufacturer, model name, color, Picture URL and
the shoe is going to have a foreign key that is related to a bin from the wardrobe.

The bin serves as a container that will hold multiple shoes.

I'm going to use polling to get the href and id of the bin the shoe resides inside of.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
