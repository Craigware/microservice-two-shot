import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function load(){
  const response1 = await fetch("http://localhost:8080/api/shoes/");
  const response2 = await fetch("http://localhost:8090/api/hats/");

  if (response1.ok && response2.ok){
    const data1 = await response1.json();
    const data2 = await response2.json();

    root.render(
      <React.StrictMode>
        <App hats={data2.hats} shoes={data1.shoes} />
      </React.StrictMode>
    )
  } else {
    console.error(response1, response2);
  }
}

load();
