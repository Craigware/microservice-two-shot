import React, {useEffect, useState} from "react";

function HatsForm() {
    const[locations, setLocations] = useState([]);

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabric(value);
    };

    const [color, setColor] = useState('');
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    };

    const [style_name, setStyleName] = useState('');
    const handleStyleChange = (e) => {
        const value = e.target.value;
        setStyleName(value);
    };

    const [picture, setPicture] = useState('');
    const handlePictureChange = (e) => {
        const value = e.target.value;
        setPicture(value);
    };

    const [location, setLocation] = useState('');
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    };

    const submit = async (event) => {
        event.preventDefault();
        const data = {};

        data.fabric = fabric;
        data.color = color;
        data.style_name = style_name;
        data.picture = picture;
        data.location = location;
        console.log(data);
        const hat_url = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(hat_url, fetchConfig)
        if (response.ok){
            const hat = await response.json();
            setFabric('');
            setColor('');
            setStyleName('');
            setPicture('');
        }

    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/');
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <h1>Add a new hat</h1>
            <form onSubmit={submit} id="hat=form">
                <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} placeholder="Fabric" required type="text" className="form-control" name="fabric" id="fabric" value={fabric} />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" className="form-control" name="color" id="color" value={color} />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStyleChange} placeholder="Style" required type="text" className="form-control" name="style" id="style" value={style_name} />
                    <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePictureChange} placeholder="Picture" required type="url" className="form-control" name="picture" id="picture" value={picture} />
                    <label htmlFor="picture">Picture</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} className="select" name="location" id="location" value={location}>
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                            );
                        })}
                        </select>
                </div>
                <button className="btn">Make</button>
            </form>
        </div>
    );

};

export default HatsForm;
