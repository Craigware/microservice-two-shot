import React, {useEffect, useState} from 'react';

function ShoesForm(){
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [bin, setBin] = useState("");
    const [picture, setPicture] = useState("");

    const handlePicture = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModel = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleBin = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = model;
        data.color = color;
        data.binVO = bin;
        data.picture = picture;
        console.log(data);

        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "shoe/json",
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok){
            const newShoe = await response.json();
            console.log(newShoe);

            setManufacturer("");
            setModel("");
            setColor("");
            setBin("");
            setPicture("");
        }

    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <form onSubmit={handleSubmit}>
            <input onChange={ handleManufacturer } value={ manufacturer } placeholder='manufacturer' required type="text" id="manufacturer" name="manufacturer" />
            <input onChange={ handleModel } value={ model } placeholder='model' required type="text" id="model" name="model" />
            <input onChange={ handleColor } value={ color } placeholder='color' required type="text" id="color" name="color" />
            <input onChange={ handlePicture } value={ picture } placeholder='http://www.pictures.com/picture' required type="url" id="picture" name="picture" />
            <select onChange={ handleBin } required id="bin" name="bin" value={bin}>
            <option value="" >Choose a bin</option>
            {bins.map(bin => {
                return (
                    <option key={ bin.href } value={ bin.href }>
                       { bin.href }
                    </option>
                )
            })}
            </select>
            <button>Create Shoe</button>
        </form>
    )
}

export default ShoesForm;
