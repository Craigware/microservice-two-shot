function ShoesList(props){

    const handleDelete = async (event) => {
        event.preventDefault();
        console.log(event.target.id);

        const url = 'http://localhost:8080/api/shoes/' + event.target.id + "/";

        const fetchConfig = {method: "delete"}
        const response = await fetch(url, fetchConfig);
        if (response.ok){
            console.log(response);
        }
        window.location.reload(false);
    }

    return (
        <table className="table table-striped table-light mt-4">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Bin</th>
                    <th>Delete?</th>
                </tr>
            </thead>

            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={ shoe.id }>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.binVO }</td>
                            <td>
                                <button id={ shoe.id } onClick={handleDelete} className="btn btn-danger btn-sm">
                                    DELETE
                                </button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;
