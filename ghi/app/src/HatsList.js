import React, {useState, useEffect} from "react";

function HatsList(props){
    const [hats, setHats] = useState([]);

    const fetchData = async () =>{
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats) ;
        }
    }
    function deleteHat(id){
        fetch(`http://localhost:8090/api/hats/${id}`,
        {method: "DELETE"})
        .then((result) => {result.json()
            .then((resp) => {console.warn(resp)
            fetchData();
        })
        window.location.reload(false);
    })
    }

    useEffect(() => {
        fetchData();
    }, [fetchData]);
    return(
        <div className="row">
            <div className="col-md-12">
                <h1>Hats</h1>
                <table className="table table-striped">
            <thead>
                <tr>
                    <th>Hat Style</th>
                    <th>Color</th>
                    <th>Fabric Type</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.location.href}</td>
                            <td>{hat.location.shelf_number}</td>
                            <td>{hat.location.section_number}</td>
                            <td><img src={hat.picture_url} alt="" height="100" /></td>
                            <td><button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
            </div>
        </div>
    );
};

export default HatsList;
