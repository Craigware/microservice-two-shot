import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import Hats,LocationVO

def poll_hats():
    response = requests.get('http://wardrobe-api:8000/api/locations/')
    content = json.loads(response.content)
    for contents in content['locations']:
        LocationVO.objects.update_or_create(
            href = contents['href'],
            defaults = {
                'closet_name':  contents['closet_name'],
                'section_number': contents['section_number'],
                'shelf_number': contents['shelf_number'],

            }
        )

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            poll_hats();
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
