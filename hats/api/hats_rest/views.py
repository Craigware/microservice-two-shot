from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'href',
        'closet_name',
        'section_number',
        'shelf_number',
    ]

class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        'id',
        'fabric',
        'style_name',
        'color',
        'picture',
        'location'
    ]
    encoders = {'location': LocationEncoder()}

    # def get_extra_data(self, o):
    #     return {"location": o.location.closet_name}

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture',
        'location'
    ]
    encoders = {'location': LocationEncoder()}

    # def get_extra_data(self, o):
    #     return {"location": o.location.id}

@require_http_methods(["GET", "POST"])
def hat_list(request, pk=None):
    if request.method == "GET":
        if pk is not None:
            hats = Hats.objects.filter(location=pk)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder= HatsEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(href=content['location'])
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'error': 'Wrong'},
                status= 400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe= False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def hat_detail(request, pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder= HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        try:
            if 'location' in content:
                location = LocationVO.objects.get(href=content['location'])
                content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'error': 'wrong'},
                status= 400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder= HatsDetailEncoder,
            sage = False,
        )
