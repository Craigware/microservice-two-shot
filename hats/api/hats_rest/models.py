from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    href = models.CharField(max_length = 100, unique = True)
    closet_name = models.CharField(max_length = 100, null=True)
    section_number = models.PositiveSmallIntegerField(null = True)
    shelf_number = models.PositiveSmallIntegerField(null = True)

class Hats(models.Model):
    fabric = models.CharField(max_length = 100)
    style_name = models.CharField(max_length = 100)
    color = models.CharField(max_length = 100)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name='location',
        on_delete=models.CASCADE,
    )
