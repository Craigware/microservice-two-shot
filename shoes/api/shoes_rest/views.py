from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
import json
from common.json import ModelEncoder


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_nam", "bin_number"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
            "manufacturer",
            "model_name",
            "id",
        ]

    def get_extra_data(self, o):
        return {"binVO": o.binVO.import_href}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
    ]
    encoders = {"binVO": BinVODetailEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, pk=None):
    if request.method == "GET":
        if pk is not None:
            shoes = Shoe.objects.filter(binVO=pk)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["binVO"]
            binVO = BinVO.objects.get(import_href=bin_href)
            content["binVO"] = binVO
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin ID"},
                status=400
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoe_detail(request, pk):
    if request.method == "GET":
        if Shoe.objects.filter(id=pk):
            shoe = Shoe.objects.filter(id=pk)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
        else:
            return JsonResponse(
                {"message": "That shoe does not exist"},
                status=400
            )

    elif request.method == "PUT":
        try:
            shoe = Shoe.objects.get(id=pk)
            content = json.loads(request.body)

            try:
                bin_href = content["binVO"]
                binVO = BinVO.objects.get(import_href=bin_href)
                content["binVO"] = binVO

            except BinVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Bin ID"},
                    status=400
                )

            shoe.update(**content)
            return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

        except Shoe.DoesNotExist:
            return JsonResponse(
                {"Message": "Shoe of that id does not exist"},
                status=404
            )
    else:
        if Shoe.objects.filter(id=pk):
            Shoe.objects.filter(id=pk).delete()
            return JsonResponse(
                {"message": "Shoe has been deleted"},
                status=200
            )
        else:
            return JsonResponse(
                {"message": "Shoe of that ID does not exist"},
                status=400
            )
