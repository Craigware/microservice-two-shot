from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=250, unique=True)
    closet_name = models.CharField(max_length=250, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=250)
    model_name = models.CharField(max_length=250)
    color = models.CharField(max_length=250)

    picture = models.URLField()
    binVO = models.ForeignKey(BinVO, related_name="shoes",
                              on_delete=models.CASCADE)
